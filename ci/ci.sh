#!/bin/sh -e

if [ "$CI_SERVER" != "yes" ]; then
    echo >&2 "Error: this script should only be run in GitLab CI!"
    exit 1
fi

echo "Creating 'ci' user"
adduser ci

CI_DIR="$(dirname $0)"
CI_CMD="${1:?}" ; shift
for target in "$@" ; do
    echo "Building $target"
    sudo -u ci "$CI_DIR"/"$CI_CMD".sh "$target"
done
