# Geanix OpenEmbedded

This repository contains the OpenEmbedded recipes and configurations used in Geanix.

## Development environment

All builds should be done using the same build environment, as defined by the
[Dockerfile](ci/Dockerfile).

### CI build

GitLab CI builds a [Docker](https://www.docker.com) image, and uses the
resulting image in its build step(s).  On successful build, the Docker image
is tagged and pushed to the [GitLab](https://gitlab.com) container registry.
This way, the GitLab container registry always hosts the latest known-working
image.

### Manual build in Toolbox container

Manual developer builds should be done using the
[Toolbox](https://github.com/containers/toolbox) image built from the
[Dockerfile](ci/Dockerfile).  Normally, you can just use the latest from the
GitLab container registry.

To create a toolbox container from GitLab container registry:

    toolbox create -c geanix-oe -i registry.gitlab.com/geanix/oe

When building older stuff, or when working on a feature branch requiring
changes to the Toolbox image, you can build the image using podman, and create
a Toolbox container from that.

     podman build -t geanix-oe-toolbox -f ci/Dockerfile ci
     toolbox create -c geanix-oe -i geanix-oe-toolbox

Next, in each shell you want to build in, you need to enter the toolbox
container, and setup the build environment:

    toolbox enter geanix-oe
    source init-build-env

And then you are ready to build stuff, for example:

    bitbake gnx-image
